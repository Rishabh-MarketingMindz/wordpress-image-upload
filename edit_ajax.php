<?php 
global $wpdb,$current_user,$post;
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require( $parse_uri[0] . 'wp-load.php' );
$data = [];
parse_str($_POST['data'],$data);
$post_id             = $data['profileid'];
$productid           = $data['productid'];
$title               = $data['your_name'];
//$your_email          = $data['your_email'];
$professional_title  = $data['professional_title'];
$location            = $data['location'];
$video               = $data['video'];
$resume_content      = $data['resume_content'];
$base                = $data['base'];
$driving_hours       = $data['driving_hours'];
$gps_localization    = $data['gps_localization'];
$jobs_gpilot         = $data['jobs_gpilot'];
$faa_atp             = $data['faa_atp'];
$easa_atp            = $data['easa_atp'];
$training_captain    = $data['training_captain'];
$licensetype1        = $data['licensetype1'];
$licensetype2        = $data['licensetype2'];
$licensetype3        = $data['licensetype3'];
$licensenumber1      = $data['licensenumber1'];
$licensenumber2      = $data['licensenumber2'];
$licensenumber3      = $data['licensenumber3'];

$medical_class1      = $data['medical_class1'];
$medical_classs1     = $data['medical_classs1'];
$medical_expiration1 = $data['medical_expiration1'];
$medical_class2      = $data['medical_class2'];
$medical_classs2     = $data['medical_classs2'];
$medical_expiration2 = $data['medical_expiration2'];
$medical_class3      = $data['medical_class3'];
$medical_classs3     = $data['medical_classs3'];
$medical_expiration3 = $data['medical_expiration3'];

$international       = $data['international'];
$passport_country1   = $data['passport_country1'];
$passport_number1    = $data['passport_number1'];
$passport_expiration1= $data['passport_expiration1'];
$passport_country2   = $data['passport_country2'];
$passport_number2    = $data['passport_number2'];
$passport_expiration2= $data['passport_expiration2'];
$passport_country3   = $data['passport_country3'];
$passport_number3    = $data['passport_number3'];
$passport_expiration3= $data['passport_expiration3'];

$visas_country1      = $data['visas_country1'];
$visas_country2      = $data['visas_country2'];
$visas_country3      = $data['visas_country3'];
$visas_number1       = $data['visas_number1'];
$visas_number2       = $data['visas_number2'];
$visas_number3       = $data['visas_number3'];
$visas_expire1       = $data['visas_expire1'];
$visas_expire2       = $data['visas_expire2'];
$visas_expire3       = $data['visas_expire3'];

$total_time         = $data['total_time'];
$rating_expiration  = $data['rating_expiration'];
$type_time          = $data['type_time'];
$landing_expiration = $data['landing_expiration'];
$schedule           = $data['schedule'];
$desired_salary     = $data['desired_salary']; 
$daily_rate_per_aircraft= $data['daily_rate_per_aircraft'];
$willing_accept_floating_job = $data['willing_accept_floating_job'];
$willing_to_relocate = $data['willing_to_relocate'];
$health_insurance = $data['health_insurance'];
$days_of_vacations_per_year = $data['days_of_vacations_per_year'];
$retirement_plan_mandatory = $data['retirement_plan_mandatory'];

$my_post = array(
    'ID'           => $post_id,
    'post_title'   => $title, // new title
);


wp_update_post( $my_post );

//update_post_meta($post_id,'gpliot_resumes',$filepath);
//update_post_meta($post_id,'your_email',$your_email);
update_post_meta($post_id,'professional_title',$professional_title);
update_post_meta($post_id,'location',$location);
update_post_meta($post_id,'video',$video);
update_post_meta($post_id,'resume_content',$resume_content);
update_post_meta($post_id,'base',$base);
update_post_meta($post_id,'driving_hours',$driving_hours);
update_post_meta($post_id,'gps_localization',$gps_localization);
update_post_meta($post_id,'jobs_gpilot',$jobs_gpilot);
update_post_meta($post_id,'faa_atp',$faa_atp);
update_post_meta($post_id,'easa_atp',$easa_atp);

update_post_meta($post_id,'licensetype1',$licensetype1);
update_post_meta($post_id,'licensetype2',$licensetype2);
update_post_meta($post_id,'licensetype3',$licensetype3);
update_post_meta($post_id,'licensenumber1',$licensenumber1);
update_post_meta($post_id,'licensenumber2',$licensenumber2);
update_post_meta($post_id,'licensenumber3',$licensenumber3);

update_post_meta($post_id,'medical_class1',$medical_class1);
update_post_meta($post_id,'medical_class2',$medical_class2);
update_post_meta($post_id,'medical_class3',$medical_class3);
update_post_meta($post_id,'medical_classs1',$medical_classs1);
update_post_meta($post_id,'medical_classs2',$medical_classs2);
update_post_meta($post_id,'medical_classs3',$medical_classs3);
update_post_meta($post_id,'medical_expiration1',$medical_expiration1);
update_post_meta($post_id,'medical_expiration2',$medical_expiration2);
update_post_meta($post_id,'medical_expiration3',$medical_expiration3);

update_post_meta($post_id,'passport_country1',$passport_country1);
update_post_meta($post_id,'passport_country2',$passport_country2);
update_post_meta($post_id,'passport_country3',$passport_country3);
update_post_meta($post_id,'passport_number1',$passport_number1);
update_post_meta($post_id,'passport_number2',$passport_number2);
update_post_meta($post_id,'passport_number3',$passport_number3);
update_post_meta($post_id,'passport_expiration1',$passport_expiration1);
update_post_meta($post_id,'passport_expiration2',$passport_expiration2);
update_post_meta($post_id,'passport_expiration3',$passport_expiration3);

update_post_meta($post_id,'visas_country1',$visas_country1);
update_post_meta($post_id,'visas_country2',$visas_country2);
update_post_meta($post_id,'visas_country3',$visas_country3);
update_post_meta($post_id,'visas_number1',$visas_number1);
update_post_meta($post_id,'visas_number2',$visas_number2);
update_post_meta($post_id,'visas_number3',$visas_number3);
update_post_meta($post_id,'visas_expire1',$visas_expire1);
update_post_meta($post_id,'visas_expire2',$visas_expire2);
update_post_meta($post_id,'visas_expire3',$visas_expire3);

update_post_meta($post_id,'international',$international);
update_post_meta($post_id,'training_captain',$training_captain);
update_post_meta($post_id,'total_time',$total_time);
update_post_meta($post_id,'rating_expiration',$rating_expiration);
update_post_meta($post_id,'type_time',$type_time);
update_post_meta($post_id,'landing_expiration',$landing_expiration);
update_post_meta($post_id,'schedule',$schedule);
update_post_meta($post_id,'desired_salary',$desired_salary);
update_post_meta($post_id,'daily_rate_per_aircraft',$daily_rate_per_aircraft);
update_post_meta($post_id,'willing_accept_floating_job',$willing_accept_floating_job);
update_post_meta($post_id,'willing_to_relocate',$willing_to_relocate);
update_post_meta($post_id,'health_insurance',$health_insurance);
update_post_meta($post_id,'days_of_vacations_per_year',$days_of_vacations_per_year);
update_post_meta($post_id,'retirement_plan_mandatory',$retirement_plan_mandatory);

$filesname = $_FILES["resume_file"]["name"];

if(!empty($_FILES["resume_file"]["name"])){
    $upload = wp_upload_bits($_FILES["resume_file"]["name"], null, file_get_contents($_FILES["resume_file"]["tmp_name"]));
    //$post_id = $post_id; //set post id to which you need to set featured image
    $filename = $upload['file'];
    $wp_filetype = wp_check_filetype($filename, null);
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit'
    );

    $attachment_id = wp_insert_attachment( $attachment, $filename, $post_id );
    if ( ! is_wp_error( $attachment_id ) ) {
        require_once(ABSPATH . 'wp-admin/includes/image.php');
        $attachment_data = wp_generate_attachment_metadata( $attachment_id, $filename );
        wp_update_attachment_metadata( $attachment_id, $attachment_data );
        set_post_thumbnail( $post_id, $attachment_id );
        update_post_meta($post_id,'gpliot_resumes',$attachment_id);
        update_post_meta($post_id,'files_name',$filesname);
    }
}

$profile_image = $_FILES["profile_image"]["name"];    
if(!empty($profile_image)){
    $upload = wp_upload_bits($_FILES["profile_image"]["name"], null, file_get_contents($_FILES["profile_image"]["tmp_name"]));
    $filenamess = $upload['file'];
    $wp_filetype = wp_check_filetype($filenamess, null);
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filenamess),
        'post_content' => '',
        'post_status' => 'inherit'
    );

    $attachment_ids = wp_insert_attachment( $attachment, $filenamess, $post_id );
    if ( ! is_wp_error( $attachment_ids ) ) {
        require_once(ABSPATH . 'wp-admin/includes/image.php');
        $attachment_data = wp_generate_attachment_metadata( $attachment_ids, $filenamess );
        wp_update_attachment_metadata( $attachment_ids, $attachment_data );
        set_post_thumbnail( $post_id, $attachment_ids );
        // update_post_meta($post_id,'gpliot_photo',$attachment_ids);
        update_post_meta($post_id,'files_photo',$filenamess);
    }
}


if($title!='' || $desired_salary !=''){
    $my_posts = array(
    'ID'           => $productid,
    'post_title'   => $title, // new title
    );
    wp_update_post( $my_posts );
    update_post_meta( $productid, '_visibility', 'visible' );
    update_post_meta( $productid, '_stock_status', 'instock');
    update_post_meta( $productid, 'total_sales', '0');
    update_post_meta( $productid, '_downloadable', 'yes');
    update_post_meta( $productid, '_virtual', 'yes');
    update_post_meta( $productid, '_regular_price', $desired_salary );
    update_post_meta( $productid, '_sale_price', "" );
    update_post_meta( $productid, '_purchase_note', "" );
    update_post_meta( $productid, '_featured', "yes" );
    update_post_meta( $productid, '_weight', "" );
    update_post_meta( $productid, '_length', "" );
    update_post_meta( $productid, '_width', "" );
    update_post_meta( $productid, '_height', "" );
    update_post_meta( $productid, '_sku', "");
    update_post_meta( $productid, '_product_attributes', array());
    update_post_meta( $productid, '_sale_price_dates_from', "" );
    update_post_meta( $productid, '_sale_price_dates_to', "" );
    update_post_meta( $productid, '_price', $desired_salary);
    update_post_meta( $productid, '_sold_individually', "" );
    update_post_meta( $productid, '_manage_stock', "no" );
    update_post_meta( $productid, '_backorders', "no" );
    update_post_meta( $productid, '_stock', "" );
   
}    

    $row = $_FILES["profile_image"]["name"];
    if($row !=''){
        $uploads = wp_upload_bits($_FILES["profile_image"]["name"], null, file_get_contents($_FILES["profile_image"]["tmp_name"]));
        $productimage = $uploads['file'];
        $wp_filetypes = wp_check_filetype($productimage, null);
        $attachments = array(
            'post_mime_type' => $wp_filetypes['type'],
            'post_title' => sanitize_file_name($productimage),
            'post_content' => '',
            'post_status' => 'inherit'
        );
        $attachment_idss = wp_insert_attachment( $attachments, $productimage, $productid );
        if ( ! is_wp_error( $attachment_idss ) ) {
            require_once(ABSPATH . 'wp-admin/includes/image.php');
            $attachment_datas = wp_generate_attachment_metadata( $attachment_idss, $productimage );
            wp_update_attachment_metadata( $attachment_idss, $attachment_datas );
            set_post_thumbnail( $productid, $attachment_idss);
            // update_post_meta($post_id,'gpliot_photo',$attachment_ids)
        }
    } 


?>
